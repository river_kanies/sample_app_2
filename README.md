# Ruby on Rails Tutorial: sample application

This is the sample application for the
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

start server: rails server -b $IP -p $PORT
start auto-testing: bundle exec guard

Notes:
"we barely ever have to think about how Rails stores data, even for production applications" (6.1.0)
-implies that user auth for db is abstracted
signup and loggin secured with ssl in ch7 (not https?)